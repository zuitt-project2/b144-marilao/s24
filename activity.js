// 2.1
db.users.find({$or: [
        {
        firstname:{$regex: "S", $options:"$i"}}, 
        
        {
        lastName: {$regex: "d", $options:"$i"}},
         
         ]
     }
        )
// 2.2
db.users.find({},
{
    _id:0,
    firstName: 1,
    lastName: 1
    }
    )
 // 3.
 // db.users.find({department: "HR"})
 db.users.find({$and: [{department:"HR"},{age: {$gte: 70}}]})
 // 4
 db.users.find({$and: [{firstName:{$regex: "e",$options: "$i"}},
{age: {$lte: 30}}]})

